import React from 'react';
import {TextInput, StyleSheet} from 'react-native';

const SearchBox = ({handleChange}) => {
  return (
    <TextInput
      placeholder="serach"
      onChangeText={handleChange}
      style={styles.box}
    />
  );
};

const styles = StyleSheet.create({
  box: {
    borderWidth: 1,
    height: 40,
    borderRadius: 20,
    marginBottom: 10,
    marginTop: 10,
    borderColor: '#999',
    padding: 10,
  },
});

export default SearchBox;
