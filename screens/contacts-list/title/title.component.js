import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {StyleSheet, View, Text} from 'react-native';

const Title = ({title, navigation}) => {
  return (
    <View style={styles.header}>
      <Text>{title}</Text>
      <Icon
        name="add-circle-outline"
        style={styles.addIcon}
        onPress={() => navigation.navigate('Add')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  addIcon: {
    fontSize: 30,
  },
});

export default Title;
