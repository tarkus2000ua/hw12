import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ContactsList from './screens/contacts-list/contacts-list.component';
import ContactDetails from './screens/contact-details/contact-details.component';
import ContactCreate from './screens/contact-create/contact-create.component';
import Title from './screens/contacts-list/title/title.component';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Contacts">
        <Stack.Screen
          name="Contacts"
          component={ContactsList}
          options={({navigation, route}) => ({
            headerTitle: (props) => (
              <Title {...props} title="Contacts" navigation={navigation} />
            ),
          })}
        />
        <Stack.Screen name="Details" component={ContactDetails} />
        <Stack.Screen name="Add" component={ContactCreate} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
