import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  LogBox,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import call from 'react-native-phone-call';

LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
]);

const ContactDetails = ({navigation, route}) => {
  const {item, onDelete} = route.params;

  return (
    <View style={styles.container}>
      <Icon name="person" style={styles.image} />
      <Text>Name</Text>
      <TextInput style={styles.input} value={item.displayName} />
      <Text>Phone number</Text>
      <TextInput style={styles.input} value={item.phoneNumbers[0]?.number} />
      <View style={styles.btnWrapper}>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            const args = {
              number: item.phoneNumbers[0]?.number,
              prompt: false,
            };
            call(args).catch(console.error);
          }}>
          <Icon name="call" style={styles.iconLeft} />
          <Text>Call</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            onDelete(item.recordID);
            navigation.goBack();
          }}>
          <Text>Delete</Text>
          <Icon name="close" style={styles.iconRight} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  image: {
    fontSize: 100,
    borderWidth: 1,
    borderColor: '#999',
    alignSelf: 'center',
    color: '#999',
  },
  input: {
    borderBottomColor: '#999',
    borderBottomWidth: 1,
    marginBottom: 25,
    fontSize: 16,
    padding: 0,
  },
  btn: {
    backgroundColor: '#00bb00',
    height: 40,
    width: 120,
    borderRadius: 20,
    color: '#fff',
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnWrapper: {
    padding: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconLeft: {
    fontSize: 20,
    marginRight: 20,
  },
  iconRight: {
    fontSize: 20,
    marginLeft: 20,
  },
});

export default ContactDetails;
