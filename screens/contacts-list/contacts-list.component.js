import React, {useEffect, useState} from 'react';
import {
  PermissionsAndroid,
  ActivityIndicator,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import Contacts from 'react-native-contacts';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SearchBox from './search-box/search-box.component';

const ContactsList = ({navigation, route}) => {
  const [contacts, setContacts] = useState([]);
  const [granted, setGranted] = useState(false);
  const [searchField, setSearchField] = useState('');
  const [loading, setloading] = useState(true);
  const [filteredContacts, setFilteredContacts] = useState([]);

  const loadContacts = () => {
    Contacts.getAll((err, contactsArr) => {
      if (err === 'denied') {
        console.warn('Permission to access contacts was denied');
      } else {
        console.log('loading');
        setContacts(contactsArr);
        setFilteredContacts(contactsArr);
        setloading(false);
      }
    });
  };

  const onDelete = (id) => {
    Contacts.deleteContact({recordID: id}, (err, recordId) => {
      if (err) {
        throw err;
      } else {
        loadContacts();
      }
    });
  };

  const addContact = (contact) => {
    const newPerson = {
      emailAddresses: [
        {
          label: 'main',
          email: contact.email,
        },
      ],
      familyName: contact.lastName,
      givenName: contact.firstName,
      phoneNumbers: [
        {
          label: 'mobile',
          number: contact.phone,
        },
      ],
    };

    Contacts.addContact(newPerson, (err) => {
      if (err) {
        throw err;
      } else {
        loadContacts();
      }
    });
  };

  if (route.params?.newContact) {
    const {newContact} = route.params;
    addContact(newContact);
    route.params.newContact = null;
  }

  useEffect(() => {
    async function requestPermissions() {
      const permissionsGranted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
      ]);
      setGranted(true);
      loadContacts();
    }
    if (!granted) {
      requestPermissions();
      console.log('request permissions');
    } else {
      console.log('granted');
    }
  });

  const handleChange = (text) => {
    setSearchField(text);
    const filtered = contacts
      .filter((item) =>
        item.displayName?.toLowerCase().includes(searchField.toLowerCase()),
      )
      .sort((a, b) => a.displayName - b.displayName);
    setFilteredContacts(filtered);
  };

  return (
    <View style={styles.container}>
      {loading === true ? (
        <View style={styles.spinner}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      ) : (
        <View>
          <SearchBox handleChange={handleChange} />
          <FlatList
            data={filteredContacts}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.contactItem}
                onPress={() =>
                  navigation.navigate('Details', {item, onDelete})
                }>
                <Icon name="perm-contact-calendar" style={styles.contactIcon} />
                <View>
                  <Text style={styles.contactName}>{item.displayName}</Text>
                  <Text style={styles.contactEmail}>
                    {item.phoneNumbers[0]?.number}
                  </Text>
                </View>
                <Icon name="info-outline" style={styles.info} />
              </TouchableOpacity>
            )}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    padding: 20,
  },
  contactItem: {
    marginBottom: 5,
    height: 60,
    backgroundColor: '#cde',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 10,
  },
  contactName: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#000',
  },
  contactEmail: {
    color: '#000',
  },
  contactIcon: {
    fontSize: 30,
    color: '#009',
    marginRight: 10,
  },
  info: {
    color: '#009',
    fontSize: 20,
    marginLeft: 'auto',
  },
  spinner: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default ContactsList;
