import React from 'react';
import {StyleSheet, View, Text, TextInput, Button} from 'react-native';
import {Formik} from 'formik';

const ContactCreate = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          phone: '',
          email: '',
        }}
        onSubmit={(newContact) =>
          navigation.navigate('Contacts', {newContact})
        }>
        {({handleChange, handleBlur, handleSubmit, values}) => (
          <View>
            <Text>First Name</Text>
            <TextInput
              placeholder="Jane"
              onChangeText={handleChange('firstName')}
              onBlur={handleBlur('firstName')}
              value={values.firstName}
            />

            <Text>Last Name</Text>
            <TextInput
              placeholder="Doe"
              onChangeText={handleChange('lastName')}
              onBlur={handleBlur('lastName')}
              value={values.lastName}
            />

            <Text>Phone</Text>
            <TextInput
              placeholder="+380 000 00 00"
              onChangeText={handleChange('phone')}
              onBlur={handleBlur('phone')}
              value={values.phone}
            />

            <Text>Email</Text>
            <TextInput
              placeholder="jane@acme.com"
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              value={values.email}
            />
            <Button onPress={handleSubmit} title="Submit" />
          </View>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  image: {
    fontSize: 100,
    borderWidth: 1,
    borderColor: '#999',
    alignSelf: 'center',
    color: '#999',
  },
  input: {
    borderBottomColor: '#999',
    borderBottomWidth: 1,
    marginBottom: 25,
    fontSize: 16,
    padding: 0,
  },
  btn: {
    backgroundColor: '#00bb00',
    height: 40,
    width: 120,
    borderRadius: 20,
    color: '#fff',
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnWrapper: {
    padding: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconLeft: {
    fontSize: 20,
    marginRight: 20,
  },
  iconRight: {
    fontSize: 20,
    marginLeft: 20,
  },
});

export default ContactCreate;
